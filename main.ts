import {
    GetProductsForIngredient,
    GetRecipes
} from "./supporting-files/data-access";
import {NutrientFact, Product, Recipe, SupplierProduct} from "./supporting-files/models";
import {
    GetCostPerBaseUnit,
    GetNutrientFactInBaseUnits
} from "./supporting-files/helpers";
import {RunTest, ExpectedRecipeSummary} from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
let recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

// will be loop through line items. 
const { recipeName, lineItems }: Recipe = recipeData[0];

const result = lineItems.map(lineItem => {
    // get products with ingredients
    const products = GetProductsForIngredient(lineItem.ingredient);

    // get cheapest supplier and nutrientFacts
    const cheapestSupplier = products
        // extract cheapest supplier for each product
        .map((product: Product) => {
            const supplierProducts = product.supplierProducts;
            const cheapOne: SupplierProduct = supplierProducts.reduce((prev, curr) => {
                const prevCost = GetCostPerBaseUnit(prev);
                const currCost = GetCostPerBaseUnit(curr);
                return prevCost > currCost ? curr : prev;
            });
            return {
                nutrientFacts: product.nutrientFacts,
                supplier: cheapOne
            };
        })
        // get cheapest supplier for entier product
        .reduce((prev, curr) => {
            const prevCost = GetCostPerBaseUnit(prev.supplier);
            const currCost = GetCostPerBaseUnit(curr.supplier);
            return prevCost > currCost ? curr : prev;
        });
    
    // normalize nutrient facts in base units
    const normalizeNutrientFacts = cheapestSupplier.nutrientFacts.map(
        (fact: NutrientFact) => GetNutrientFactInBaseUnits(fact)
    );

    // base price for each supplier product 
    const productUnitPrice: number = GetCostPerBaseUnit(cheapestSupplier.supplier);
    
    // total price
    const productPrice = productUnitPrice * lineItem.unitOfMeasure.uomAmount;
    return {
        normalizeNutrientFacts,
        productPrice
    }
})
// get total price of supplier products
// and concat all nutrient facts
.reduce((acc, value) => {
    return {
        productPrice: acc.productPrice + value.productPrice,
        normalizeNutrientFacts: acc.normalizeNutrientFacts.concat(value.normalizeNutrientFacts)
    }
}, {productPrice: 0, normalizeNutrientFacts: []})

// merge nutrient facts based on name, for example, Fat, Sodium and so on
const unorderedNutrientsAtCheapestCost = result.normalizeNutrientFacts.reduce((acc, obj) => {
    const key = obj['nutrientName'];
    let sumGroup = {};
    if(acc[key]) {
        sumGroup = {
            ...acc[key],
            quantityAmount: {
                ...acc[key].quantityAmount,
                uomAmount: acc[key].quantityAmount.uomAmount + obj.quantityAmount.uomAmount
            }
        }
    } else {
        sumGroup = obj;
    }
    return { ...acc, [key]: sumGroup}
}, {})

// sort based on nutrient name
const sortObject = (obj: any) => Object.keys(obj).sort().reduce((res, key) => (res[key] = obj[key], res), {});
let nutrientsAtCheapestCost = sortObject(unorderedNutrientsAtCheapestCost); 

// build summary objects
recipeSummary[recipeName] = {
    cheapestCost: result.productPrice,
    nutrientsAtCheapestCost
}
/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
